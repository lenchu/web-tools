const priceCalculateTemplate =
`
<div>
<div style="display: flex; justify-content: space-between;">
  <el-input placeholder="priceOne" v-model="p1" />
  <span style="width: 50px;"></span>
  <el-input placeholder="priceTwo" v-model="p2" />
</div>
<el-descriptions direction="vertical" :column="4" border>
  <el-descriptions-item label="上涨">{{rise}} %</el-descriptions-item>
  <el-descriptions-item label="差值">{{diff}}</el-descriptions-item>
  <el-descriptions-item label="下跌">{{fall}} %</el-descriptions-item>
</el-descriptions>
</div>
`

Vue.component('price-calculate', {
  props: {
  },
  data: function () {
    return {
      p1: undefined,
      p2: undefined
    }
  },
  computed: {
    n1: function () {
      return parseFloat(this.p1)
    },
    n2: function () {
      return parseFloat(this.p2)
    },
    diff: function () {
      return (this.n2 - this.n1).toFixed(3)
    },
    rise: function () {
      return (this.diff * 100 / this.n1).toFixed(2)
    },
    fall: function () {
      return (this.diff * 100 / this.n2).toFixed(2)
    }
  },
  methods: {
  },
  template: priceCalculateTemplate
})
